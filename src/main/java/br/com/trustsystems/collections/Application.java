package br.com.trustsystems.collections;

import br.com.trustsystems.collections.list.TestListPerformance;
import br.com.trustsystems.collections.map.TestMapPerformance;

public class Application {
    public static void main(final String[] args) {
        String type = "map";
        if (args.length == 1) {
            type = args[0];
        }

        switch (type.toLowerCase()) {
            case "map":
                TestMapPerformance.run();
                break;
            case "list":
                TestListPerformance.run();
                break;
            default:
                System.out.println("Wrong argument: [map|list]");
        }

    }
}
